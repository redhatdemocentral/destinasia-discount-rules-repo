package techoffice;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class CarResp implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String carCity;
	private java.lang.Integer carDays;
	private java.lang.Integer carDisc;
	private java.lang.Double carPrice;
	private java.lang.String carRentalCo;
	private java.lang.String carResCode;
	private java.lang.String carResStatus;
	private java.lang.String carStartDate;
	private java.lang.String carType;

	public CarResp() {
	}

	public java.lang.String getCarCity() {
		return this.carCity;
	}

	public void setCarCity(java.lang.String carCity) {
		this.carCity = carCity;
	}

	public java.lang.Integer getCarDays() {
		return this.carDays;
	}

	public void setCarDays(java.lang.Integer carDays) {
		this.carDays = carDays;
	}

	public java.lang.Integer getCarDisc() {
		return this.carDisc;
	}

	public void setCarDisc(java.lang.Integer carDisc) {
		this.carDisc = carDisc;
	}

	public java.lang.Double getCarPrice() {
		return this.carPrice;
	}

	public void setCarPrice(java.lang.Double carPrice) {
		this.carPrice = carPrice;
	}

	public java.lang.String getCarRentalCo() {
		return this.carRentalCo;
	}

	public void setCarRentalCo(java.lang.String carRentalCo) {
		this.carRentalCo = carRentalCo;
	}

	public java.lang.String getCarResCode() {
		return this.carResCode;
	}

	public void setCarResCode(java.lang.String carResCode) {
		this.carResCode = carResCode;
	}

	public java.lang.String getCarResStatus() {
		return this.carResStatus;
	}

	public void setCarResStatus(java.lang.String carResStatus) {
		this.carResStatus = carResStatus;
	}

	public java.lang.String getCarStartDate() {
		return this.carStartDate;
	}

	public void setCarStartDate(java.lang.String carStartDate) {
		this.carStartDate = carStartDate;
	}

	public java.lang.String getCarType() {
		return this.carType;
	}

	public void setCarType(java.lang.String carType) {
		this.carType = carType;
	}

	public CarResp(java.lang.String carCity, java.lang.Integer carDays,
			java.lang.Integer carDisc, java.lang.Double carPrice,
			java.lang.String carRentalCo, java.lang.String carResCode,
			java.lang.String carResStatus, java.lang.String carStartDate,
			java.lang.String carType) {
		this.carCity = carCity;
		this.carDays = carDays;
		this.carDisc = carDisc;
		this.carPrice = carPrice;
		this.carRentalCo = carRentalCo;
		this.carResCode = carResCode;
		this.carResStatus = carResStatus;
		this.carStartDate = carStartDate;
		this.carType = carType;
	}

}